
  

# About the project

Start your wordpress project very quickly and easy way with docker compose, to run the project, please open your terminal and run the command below.
```
docker-compose up --build
```
After that just open your browser and start to use with the address `http://localhost:8002`. One of the reasons why you should use wordpress this way is because now you can start to version control of your project since the database and the code exists in the same folder.

# About the docker and docker compose

Let's talk something about docker and doubts about it. :)

## common commands

We have a lot of docker commands very usefull when we are working usually, below are some of these commands and what they do:

 - **$ docker run**: run a Dockerfile in the current folder of terminal;
 - **$ docker ps**: list all the containers that are activated at the moment (if you want list all the container that your machine have you need run **$ docker ps -a**);
 - **$ docker exec -it CONTEINER_ID /bin/bash**: this command is very usefull when you want to access inside your container and see the structure of foldes and files for example;
 - **$ docker stop CONTEINER_ID**: command used to stop one of more containers activateds;
 - **$ docker images**: list all your images that you have in your machine;
 - **$ docker build -t IMAGE_NAME .**: use this command to build your image (check if the Dockerfile exists in the current folder of terminal);
 - **$ docker login**: command used when you want log in in the [docker hub](https://hub.docker.com/);
 - **$ docker push IMAGE_NAME**: command used to push an local image to docker hub (check if you are already logged);
 - **$ docker pull IMAGE_NAME**: get an image from docker hub to your machine;
 - **$ docker-compose up**: this command is used when you have an docker-compose.yml and want to run all services that are defined in your docker-compose.yml file (if you are executing this command in the docker-compose to the first time, please execute the command **$ docker-compose up --build** to create all images and services);
 - **$ docker-compose ps**: list all the docker composers activated;

## what's the difference between images and containers

**Images** can exist without **containers**, whereas a **container** needs to run an **image** to exist. Therefore, **containers** are dependent on **images** and use them to construct a run-time environment and run an application. The two concepts exist as essential components (or rather phases) in the process of running a Docker **container**.
 
# useful links

- https://docs.docker.com/compose/wordpress/
- https://github.com/nezhar/wordpress-docker-compose
- https://hub.docker.com/
- https://phoenixnap.com/kb/docker-image-vs-container#:~:text=Images%20can%20exist%20without%20containers,of%20running%20a%20Docker%20container.